# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0

import os
import uuid

from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


def get_upload_path_feedback(instance, filename):
    # You can modify this function to determine the path based on instance attributes
    base_dir = "/Users/hadizadeh-m/stac/files/feedback"
    os.makedirs(base_dir, exist_ok=True)

    return os.path.join("files/feedback/", filename)


# Extending User Model Using a One-To-One Link
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    # avatar = models.ImageField(default='default.jpg', upload_to='profile_images')
    # bio = models.TextField()

    def __str__(self):
        return self.user.username


@receiver(post_save, sender=User)
def create_profile(sender, instance, created, **kwargs):
    if created:
        u = Profile.objects.create(user=instance)
        u.is_active = True


@receiver(post_save, sender=User)
def save_profile(sender, instance, **kwargs):
    Profile.objects.get_or_create(user=instance)
    instance.profile.save()


class AAITokenModel(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    email = models.CharField(max_length=500, blank=False, null=False)
    access_token = models.CharField(max_length=50000, blank=False, null=False)
    refresh_token = models.CharField(max_length=50000, blank=False, null=False)
    expires_in = models.CharField(max_length=500, blank=False, null=False)
    token_type = models.CharField(max_length=500, blank=False, null=False)
    scope = models.CharField(max_length=500, blank=False, null=False)
    expires_at = models.CharField(max_length=500, blank=False, null=False)

    def __str__(self):
        return self.user.username


class TokenModel(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    email = models.CharField(max_length=500, blank=False, null=False)
    access_token = models.CharField(max_length=50000, blank=False, null=False)
    refresh_token = models.CharField(max_length=50000, blank=False, null=False)
    expires_in = "4000"
    token_type = "Bearer"
    expires_at = models.CharField(max_length=500, blank=False, null=False)

    def __str__(self):
        return self.user.username


class FeedBack(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=500, blank=False, null=False)
    email = models.CharField(max_length=500, blank=False, null=False)
    message = models.CharField(max_length=50000, blank=False, null=False)
    feedback_img = models.ImageField(
        upload_to=get_upload_path_feedback,
        blank=True,
        null=True,
        max_length=5000,
    )

    def __str__(self):
        return self.title
