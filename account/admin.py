# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

from .forms import CustomUserChangeForm
from .models import AAITokenModel, Profile, TokenModel

admin.site.register(Profile)

admin.site.register(AAITokenModel)

admin.site.register(TokenModel)


class UserAdmin(BaseUserAdmin):
    form = CustomUserChangeForm

    def get_fieldsets(self, request, obj=None):
        # If the user is a superuser, return the default fieldsets
        if request.user.is_superuser:
            return super().get_fieldsets(request, obj)
        # For non-superusers, restrict the fieldsets to only include the password field
        return ((None, {"fields": ("password",)}),)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs  # Superusers get full access
        return qs.filter(
            id=request.user.id
        )  # Regular users can only see/edit their own instance

    # Override other fieldsets to remove fields if necessary


# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
