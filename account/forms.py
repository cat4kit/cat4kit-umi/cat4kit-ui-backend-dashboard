# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0

from django import forms
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.contrib.auth.models import User
from django.forms import ModelForm

from .models import FeedBack


class SignupForm(UserCreationForm):
    class Meta:
        model = User
        fields = (
            "email",
            "username",
            "password1",
            "password2",
        )


class ProfileForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ("email", "username")


class FeedbackForm(ModelForm):
    class Meta:
        model = FeedBack
        fields = ("email", "title", "message", "feedback_img")


class CustomUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):  # type: ignore
        model = User
        fields = ("password",)
