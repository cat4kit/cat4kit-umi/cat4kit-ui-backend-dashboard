import os
from datetime import datetime
from typing import Any, Dict, List

import requests
from authlib.integrations.django_client import OAuth
from django.conf import settings

# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0
from django.contrib.auth import authenticate, get_user_model
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.models import Permission, User
from django.contrib.contenttypes.models import ContentType
from django.core.mail import EmailMessage
from django.http import JsonResponse
from django.shortcuts import redirect
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes, force_str
from django.utils.functional import cached_property
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django_helmholtz_aai.views import HelmholtzAuthentificationView
from rest_framework.decorators import (
    api_view,
    authentication_classes,
    permission_classes,
)
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.tokens import RefreshToken

from .forms import FeedbackForm, ProfileForm, SignupForm
from .models import AAITokenModel, TokenModel
from .serializers import (
    AAITokenModelSerializer,
    TokenModelSerializer,
    UserSerializer,
)
from .tokens import account_activation_token


@api_view(["GET"])
def me(request):
    return JsonResponse(
        {
            "id": request.user.id,
            "email": request.user.email,
            "username": request.user.username,
        }
    )


def activate(request, uidb64, token):
    User = get_user_model()
    try:
        uid = force_str(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except Exception:
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.is_staff = True
        # Get the content type for the User model
        content_type = ContentType.objects.get_for_model(User)

        # Get the 'change_user' permission
        change_user_permission = Permission.objects.get(
            codename="change_user",
            content_type=content_type,
        )

        # Add the permission to the user
        user.user_permissions.add(change_user_permission)
        user.save()

    return redirect(settings.FRONTEND_URL)


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def signup(request):
    data = request.data
    if data.get("email") in settings.ALLOWED_EMAILS:
        message = "success"
        form = SignupForm(
            {
                "email": data.get("email"),
                "username": data.get("name"),
                "password1": data.get("password1"),
                "password2": data.get("password2"),
            }
        )
        print(form.errors.as_json())
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False

            user.save()

            mail_subject = "Activate your user account."
            messagez = render_to_string(
                "template_activate_account.html",
                {
                    "user": user.username,
                    "domain": settings.ROOT_URL,
                    "uid": urlsafe_base64_encode(force_bytes(user.pk)),
                    "token": account_activation_token.make_token(user),
                    "protocol": "https" if request.is_secure() else "http",
                },
            )
            email = EmailMessage(mail_subject, messagez, to=[user.email])
            email.content_subtype = "html"  # Main content is now text/html
            email.send()

        else:
            message = form.errors.as_json()
            return JsonResponse({"message": message})
    else:
        return JsonResponse(
            {
                "message": "Your email is not allowed to register. Please contact the administrator."
            },
            safe=False,
        )


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def editprofile(request):
    data = request.data
    user = request.user
    if (
        User.objects.filter(email=data.get("email"))
        .exclude(pk=user.id)
        .exists()
    ):
        return JsonResponse({"message": "email already exists"})
    else:
        form = ProfileForm(request.POST, request.FILES, instance=user)
        if form.is_valid():
            form.save()
        serializer = UserSerializer(user)
        return JsonResponse(
            {
                "message": "Information updated successfully",
                "user": serializer.data,
            }
        )


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def editpassword(request):
    user = request.user
    form = PasswordChangeForm(data=request.POST, user=user)
    print(form.errors.as_json())
    if form.is_valid():
        form.save()
        return JsonResponse({"message": "success"})
    else:
        return JsonResponse({"message": form.errors.as_json()}, safe=False)


class HelmholtzTokenObtainPairRefreshView(
    HelmholtzAuthentificationView, APIView
):
    oauth = OAuth()
    oauth.register(name="helmholtz", **settings.HELMHOLTZ_CLIENT_KWS)

    @cached_property
    def token(self) -> Dict[str, Any]:
        return self.oauth.helmholtz.authorize_access_token(self.request)

    @cached_property
    def userinfo(self) -> Dict[str, Any]:
        return self.oauth.helmholtz.userinfo(
            request=self.request, token=self.token
        )

    authentication_classes: List[str] = []
    permission_classes: List[str] = []

    def get(self, request, format=None) -> Response:
        """For listing out the posts, HTTP method: GET"""

        user_info = self.userinfo
        user_object = self.get_user_from_email(self.userinfo["email"])

        if self.is_new_user and user_object is None:
            self.aai_user = self.create_user(self.userinfo)
        else:
            self.aai_user = user_object  # type: ignore
            self.update_user()

        self.synchronize_vos()
        if self.aai_user is not None:
            self.login_user(self.aai_user)
        user_info["redirect_url"] = settings.LOGIN_REDIRECT_URL
        aaitoken_dict = {
            "access_token": self.token["access_token"],
            "refresh_token": self.token["refresh_token"],
            "expires_in": self.token["expires_in"],
            "expires_at": self.token["expires_at"],
            "token_type": self.token["token_type"],
            "scope": self.token["scope"],
            "email": user_info["email"],
            "user": self.aai_user,
        }
        # create or update aaitoken
        AAITokenModel.objects.update_or_create(
            email=user_info["email"],
            defaults=aaitoken_dict,
        )
        user = User.objects.get(email=user_info["email"])
        if (
            user_info.get("eduperson_entitlement") is not None
            and settings.HELMHOLTZ_ALLOWED_VOS_CAT4KIT[0]
            in user_info["eduperson_entitlement"]
        ):
            print(user_info["eduperson_entitlement"])
            if not user.is_superuser:
                user.is_active = True
                user.is_staff = True
                # Get the content type for the User model
                content_type = ContentType.objects.get_for_model(User)  # type: ignore
                # Get the 'change_user' permission
                change_user_permission = Permission.objects.get(
                    codename="change_user",
                    content_type=content_type,
                )
                # Add the permission to the user
                user.user_permissions.add(change_user_permission)
                user.save()
            return redirect(
                settings.LOGIN_REDIRECT_URL
                + f"?email={user_info['email']}&aai=true"
            )
        else:
            user.is_active = False
            user.is_staff = False
            return redirect(
                settings.FRONTEND_URL
                + "/#/auth/login/Your email address is not recognized as member of the Cat4KIT Resource Capability. To gain access to Cat4KIT-UMI, please reach out to christof.lorenz@kit.edu."
            )


#   if user_info["email_verified"] is True:
#             user_info["email_verified"] = "true"
#         if user_info["email_verified"] is False:
#             user_info["email_verified"] = "false"
#         print(settings.LOGIN_REDIRECT_URL)
#         user_info["redirect_url"] = settings.LOGIN_REDIRECT_URL
#         user_info["frontend_url"] = settings.FRONTEND_URL
#         return JsonResponse(self.token)
# print(self.token)

# if (
#     settings.HELMHOLTZ_ALLOWED_VOS_CAT4KIT[0]
#     in user_info["eduperson_entitlement"]
# ):

#     return render(
#         request, "token_career.html", {**user_info, **self.token}
#     )
# else:
# return redirect(settings.FRONTEND_URL)


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def helmholtz_aai_refresh_token(request):
    object = AAITokenModel.objects.filter(email=request.data["email"]).first()
    current_timestamp = int(datetime.now().timestamp())
    if object is not None and current_timestamp < int(object.expires_at):
        object_serializer = AAITokenModelSerializer(object)
        return JsonResponse(object_serializer.data, safe=False)
    else:
        return JsonResponse(
            {
                "message": "The token has expired or your email does not have sufficient access privileges for Cat4KIT-UMI."
            },
            safe=False,
        )


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def save_login_access_refresh_token(request):
    try:
        email = request.data.get("email")
        password = request.data.get("password")
        username = email.split("@")[0]

        # Authenticate user
        user_object = authenticate(username=username, password=password)

        if not user_object:
            return JsonResponse({"error": "Invalid credentials"}, status=401)

        # Generate tokens manually using RefreshToken
        refresh = RefreshToken.for_user(user_object)
        access_token = str(refresh.access_token)
        refresh_token = str(refresh)

        expires_at = int(datetime.now().timestamp()) + 4000

        token_dict = {
            "access_token": access_token,
            "refresh_token": refresh_token,
            "expires_at": expires_at,
            "user": user_object,
            "email": email,
        }

        # Save or update token information
        TokenModel.objects.update_or_create(
            email=email,
            defaults=token_dict,
        )

        return JsonResponse({"message": "Tokens saved successfully"})

    except Exception as e:
        return JsonResponse({"error": str(e)}, status=400)


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def refresh_token(request):
    object = TokenModel.objects.filter(email=request.data["email"]).first()
    current_timestamp = int(datetime.now().timestamp())
    if object is not None and current_timestamp < int(object.expires_at):
        object_serializer = TokenModelSerializer(object)
        return JsonResponse(object_serializer.data, safe=False)
    else:
        return JsonResponse(
            {
                "message": "The token has expired or your email does not have sufficient access privileges for Cat4KIT-UMI."
            },
            safe=False,
        )


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def send_feedback(request):
    # data = request.data
    # email_to_ = "gitlab-incoming+cat4kit-issues-harvester-9934-8zagoh8g9bfch7qvvhrw08up4-issue@hzdr.de"
    # email_to_1 = "mostafa.hadizadeh@kit.edu"

    mail_subject = request.data["title"]
    messagez = request.data["message"]
    form = FeedbackForm(request.POST, request.FILES)
    if form.is_valid():
        feedback_instance = form.save()
        image_location = (
            feedback_instance.feedback_img.name
            if feedback_instance.feedback_img
            else None
        )
    else:
        return JsonResponse({"message": form.errors.as_json()}, safe=False)
    email = request.data["email"]

    repo_token = settings.GITLAB_TOKEN
    headers = {"Private-Token": repo_token}
    stac_files = settings.STAC_FILES
    os.makedirs(stac_files, exist_ok=True)
    print("Im here")
    if image_location:
        files = {
            "file": open(
                "/app/stac/" + str(image_location),
                "rb",
            ),
        }

        upload_url = (
            "https://codebase.helmholtz.cloud/api/v4/projects/9934/uploads"
        )
        # Upload the image file
        upload_response = requests.post(
            upload_url, headers=headers, files=files
        )

        # Get the uploaded file URL
        if upload_response.status_code == 201:
            uploaded_file_url = upload_response.json()["url"]
            # Construct the issue payload
            issue_payload = {
                "title": mail_subject,
                "description": f"{messagez}\n\n![Uploaded Image]({uploaded_file_url}) \n\n  {email}",
                "confidential": True,
            }
        else:
            return JsonResponse(
                {
                    "message": "Error uploading file. Status code: {upload_response.status_code}"
                }
            )
    else:
        # Construct the issue payload
        issue_payload = {
            "title": mail_subject,
            "description": f"{messagez} \n\n  {email}",
            "confidential": True,
        }

    # url = (
    #     "https://codebase.helmholtz.cloud/api/v4/projects/9934/issues?title="
    #     + mail_subject
    #     + "&confidential=true&description="
    #     + messagez
    # )
    url = "https://codebase.helmholtz.cloud/api/v4/projects/9934/issues"
    req = requests.post(url, headers=headers, json=issue_payload)

    return JsonResponse(
        {"message": str(req.status_code) + ": " + str(req.reason)}
    )
