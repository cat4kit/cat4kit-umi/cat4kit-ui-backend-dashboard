# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0

from django.contrib.auth.models import User
from rest_framework import serializers

from .models import AAITokenModel, TokenModel


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            "id",
            "username",
            "email",
        )


class AAITokenModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = AAITokenModel
        fields = "__all__"


class TokenModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = TokenModel
        fields = "__all__"
