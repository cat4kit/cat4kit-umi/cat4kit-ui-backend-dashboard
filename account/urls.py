# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0

from django.urls import include, path
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

from . import api
from .api import HelmholtzTokenObtainPairRefreshView

urlpatterns = [
    path("signup/", api.signup, name="signup"),
    path("login/", TokenObtainPairView.as_view(), name="token_obtain"),
    path("send_feedback/", api.send_feedback, name="send_feedback"),
    path("refresh/", TokenRefreshView.as_view(), name="token_refresh"),
    path(
        "helmholtz_aai_refresh_token/",
        api.helmholtz_aai_refresh_token,
        name="helmholtz_aai_refresh_token",
    ),
    path(
        "save_login_access_refresh_token/",
        api.save_login_access_refresh_token,
        name="save_login_access_refresh_token",
    ),
    path("refresh_token/", api.refresh_token, name="refresh_token"),
    path("editprofile/", api.editprofile, name="editprofile"),
    path("editpassword/", api.editpassword, name="editpassword"),
    path("me/", api.me, name="me"),
    path(
        "helmholtz-aai/auth/",
        HelmholtzTokenObtainPairRefreshView.as_view(),
    ),
    path("helmholtz-aai/", include("django_helmholtz_aai.urls")),
    path("activate/<uidb64>/<token>", api.activate, name="activate"),
]
