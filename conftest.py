# SPDX-FileCopyrightText: 2023 KIT - Karlsruher Institut für Technologie
#
# SPDX-License-Identifier: EUPL-1.2

"""pytest configuration script for cat4kit-ui-backend-dashboard."""

import pytest  # noqa: F401
