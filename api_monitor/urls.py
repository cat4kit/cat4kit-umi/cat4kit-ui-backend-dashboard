# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0

from django.urls import path

from . import api

urlpatterns = [
    path("api_info/", api.api_info, name="api_info"),
    path("dashboard/<str:email>/", api.dashboard, name="dashboard"),
]
