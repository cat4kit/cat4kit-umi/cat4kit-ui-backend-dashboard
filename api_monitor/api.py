# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0

from datetime import datetime, timedelta

import requests
from django.http import JsonResponse
from drf_api_logger.models import APILogsModel
from rest_framework.decorators import (
    api_view,
    authentication_classes,
    permission_classes,
)


@api_view(["GET"])
@authentication_classes([])
@permission_classes([])
def api_info(request):
    result_for_200 = APILogsModel.objects.filter(status_code=200)
    result_for_301 = APILogsModel.objects.filter(status_code=301)
    result_for_404 = APILogsModel.objects.filter(status_code=404)
    result_for_500 = APILogsModel.objects.filter(status_code=500)

    today = datetime.now()
    today_calls = APILogsModel.objects.filter(
        added_on__year=today.year,
        added_on__month=today.month,
        added_on__day=today.day,
    )
    yesterday = today - timedelta(days=1)
    yesterday_calls = APILogsModel.objects.filter(
        added_on__year=yesterday.year,
        added_on__month=yesterday.month,
        added_on__day=yesterday.day,
    )
    two_days_ago = today - timedelta(days=2)
    two_days_ago_calls = APILogsModel.objects.filter(
        added_on__year=two_days_ago.year,
        added_on__month=two_days_ago.month,
        added_on__day=two_days_ago.day,
    )
    three_days_ago = today - timedelta(days=3)
    three_days_ago_calls = APILogsModel.objects.filter(
        added_on__year=three_days_ago.year,
        added_on__month=three_days_ago.month,
        added_on__day=three_days_ago.day,
    )
    four_days_ago = today - timedelta(days=4)
    four_days_ago_calls = APILogsModel.objects.filter(
        added_on__year=four_days_ago.year,
        added_on__month=four_days_ago.month,
        added_on__day=four_days_ago.day,
    )
    return JsonResponse(
        {
            "APICall_x": [
                four_days_ago.strftime("%Y-%m-%d"),
                three_days_ago.strftime("%Y-%m-%d"),
                two_days_ago.strftime("%Y-%m-%d"),
                yesterday.strftime("%Y-%m-%d"),
                today.strftime("%Y-%m-%d"),
            ],
            "APICall_y": [
                four_days_ago_calls.count(),
                three_days_ago_calls.count(),
                two_days_ago_calls.count(),
                yesterday_calls.count(),
                today_calls.count(),
            ],
            "status_codes": ["HTTP 200", "HTTP 301", "HTTP 404", "HTTP 500"],
            "status_codes_num": [
                result_for_200.count(),
                result_for_301.count(),
                result_for_404.count(),
                result_for_500.count(),
            ],
        }
    )


@api_view(["GET"])
@authentication_classes([])
@permission_classes([])
def dashboard(request, email):
    """
    Retrieve a dashboard report summary for test jobs and scheduler jobs.

    This function fetches data from two different APIs, each corresponding to test jobs
    and scheduler jobs, and provides a summary of their statuses. It includes counts of
    various statuses like harvested, ingested, and failed jobs.

    Parameters:
    - request (HttpRequest): The HttpRequest object.
    - email (str): The email address to filter the dashboard report.

    Returns:
    - JsonResponse: A summary of the test jobs and scheduler jobs dashboard report.

    Example:
    GET /api/dashboard?email=user@example.com
    """

    # Fetching test jobs and scheduler jobs data
    response_test = requests.get(
        f"https://cat4kit.atmohub.kit.edu/test/api/get_testjob_objects/{email}/"
    )
    response_scheduler = requests.get(
        "https://cat4kit.atmohub.kit.edu/scheduler/api/get_schedulerjob_objects/"
    )

    response_test_json = response_test.json()
    response_scheduler_json = response_scheduler.json()

    # Counting statuses in test jobs
    harvested_test_count = sum(
        not isinstance(job, str)
        and job.get("harvesting_status") in ["Finished", "Started"]
        for job in response_test_json
    )
    failed_harvested_test_jobs_count = sum(
        not isinstance(job, str) and job.get("harvesting_status") == "Failed"
        for job in response_test_json
    )
    ingested_test_count = sum(
        not isinstance(job, str)
        and job.get("ingesting_status") not in [None, "pending"]
        for job in response_test_json
    )
    failed_ingested_test_jobs_count = sum(
        not isinstance(job, str) and job.get("ingesting_status") == "Failed"
        for job in response_test_json
    )
    failed_test_count = sum(
        not isinstance(job, str) and "Failed" in job.get("general_status")
        for job in response_test_json
    )

    # Counting statuses in scheduler jobs
    harvested_scheduler_count = sum(
        not isinstance(job, str)
        and job.get("harvesting_status") in ["Finished", "Started"]
        for job in response_scheduler_json
    )
    failed_harvested_scheduler_count = sum(
        not isinstance(job, str) and job.get("harvesting_status") == "Failed"
        for job in response_scheduler_json
    )
    ingested_scheduler_count = sum(
        not isinstance(job, str)
        and job.get("ingesting_status") not in [None, "pending"]
        for job in response_scheduler_json
    )
    failed_ingested_scheduler_count = sum(
        not isinstance(job, str) and job.get("ingesting_status") == "Failed"
        for job in response_scheduler_json
    )
    failed_scheduler_count = sum(
        not isinstance(job, str) and "Failed" in job.get("general_status")
        for job in response_scheduler_json
    )

    # Creating the response summary
    dashboard_summary = {
        "harvested": {
            "total": harvested_test_count + harvested_scheduler_count,
            "failed_harvested": failed_harvested_test_jobs_count
            + failed_harvested_scheduler_count,
            "successful_harvested": harvested_test_count
            + harvested_scheduler_count
            - (
                failed_harvested_test_jobs_count
                + failed_harvested_scheduler_count
            ),
        },
        "ingested": {
            "total": ingested_test_count + ingested_scheduler_count,
            "failed_ingested": failed_ingested_test_jobs_count
            + failed_ingested_scheduler_count,
            "successful_ingested": ingested_test_count
            + ingested_scheduler_count
            - (
                failed_ingested_test_jobs_count
                + failed_ingested_scheduler_count
            ),
        },
        "test_jobs": {
            "total": len(response_test_json),
            "failed": failed_test_count,
            "successfull": len(response_test_json) - failed_test_count,
        },
        "scheduler_jobs": {
            "total": len(response_scheduler_json),
            "failed": failed_scheduler_count,
            "successfull": len(response_scheduler_json)
            - failed_scheduler_count,
        },
    }

    return JsonResponse(dashboard_summary, safe=False)
