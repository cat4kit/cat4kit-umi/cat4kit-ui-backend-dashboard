# SPDX-FileCopyrightText: 2023 KIT - Karlsruher Institut für Technologie
#
# SPDX-License-Identifier: CC0-1.0

"""Setup script for the cat4kit-ui-backend-dashboard package."""
import versioneer
from setuptools import setup

setup(
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
)
