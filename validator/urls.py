# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0

from django.urls import path

from . import api

urlpatterns = [
    # path("tds2stac_harvesting_ingesting/", api.tds2stac_harvesting_ingesting, name="tds2stac_harvesting_ingesting"),
    path(
        "get_all_available_webservices/",
        api.get_all_available_webservices,
        name="get_all_available_webservices",
    ),
    path(
        "tds2stac_validate_catalog_url/",
        api.tds2stac_validate_catalog_url,
        name="tds2stac_validate_catalog_url",
    ),
]
