# SPDX-FileCopyrightText: 2023 "2023 Karlsruher Institut für Technologie"
#
# SPDX-License-Identifier: CC0-1.0


import traceback

import requests
from django.http import JsonResponse
from rest_framework.decorators import (
    api_view,
    authentication_classes,
    permission_classes,
)
from tds2stac import WebServiceListScraper


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def get_all_available_webservices(request):
    """Get all available webservices from the catalog."""

    catalog_url = request.data["catalog_url"].strip().replace("\n", "")
    username = (
        request.data["catalog_url_username"].strip().replace("\n", "")
        if request.data.get("catalog_url_username") is not None
        else None
    )
    password = (
        request.data["catalog_url_password"].strip().replace("\n", "")
        if request.data.get("catalog_url_password") is not None
        else None
    )
    ssl_verify = request.data["ssl_verify"]
    timeout = request.data["timeout"]
    try:
        webservices = WebServiceListScraper(
            url=catalog_url,
            requests_properties={
                "verify": ssl_verify,
                "timeout": timeout,
                "auth": (username, password),
            },
        )
        return JsonResponse(
            {
                "message": "success",
                "webservices": list(webservices),
            },
        )
    except Exception:
        print(traceback.format_exc())
        return JsonResponse(
            {
                "message": "failed in getting available webservices",
            },
            safe=False,
        )


@api_view(["POST"])
@authentication_classes([])
@permission_classes([])
def tds2stac_validate_catalog_url(request):
    """Get all available webservices from the catalog."""

    catalog_url = request.data["catalog_url"].strip().replace("\n", "")
    username = (
        request.data["catalog_url_username"].strip().replace("\n", "")
        if request.data.get("catalog_url_username") is not None
        else None
    )
    password = (
        request.data["catalog_url_password"].strip().replace("\n", "")
        if request.data.get("catalog_url_password") is not None
        else None
    )
    ssl_verify = request.data["ssl_verify"]
    timeout = request.data["timeout"]

    if catalog_url.isspace() or catalog_url == "":
        return JsonResponse(
            {"TDS_status": "TDS Checker msg: No catalog URL provided."}
        )
    elif (
        not catalog_url.isspace()
        and catalog_url != ""
        and not username.isspace()
        and username != ""
        and not password.isspace()
        and password != ""
    ):
        try:
            xml_url = requests.get(
                catalog_url,
                # None,
                auth=(username, password),
                verify=ssl_verify,
                timeout=timeout,
            )
            if xml_url.status_code == 200:
                return JsonResponse(
                    {
                        "TDS_status": "Thredds Data Server is accessible.",
                    },
                )
            else:
                return JsonResponse(
                    {
                        "TDS_status": "Thredds Data Server is not accessible. Contact TDS administrator.",
                    },
                    safe=False,
                )
        except BaseException:
            return JsonResponse(
                {
                    "TDS_status": "Thredds Data Server is not accessible. Contact TDS administrator.",
                },
                safe=False,
            )
    else:
        try:
            xml_url = requests.get(
                catalog_url,
                # None,
                # auth=(username, password),
                verify=ssl_verify,
                timeout=timeout,
            )
            if xml_url.status_code == 200:
                return JsonResponse(
                    {
                        "TDS_status": "Thredds Data Server is accessible.",
                    },
                )
            else:
                return JsonResponse(
                    {
                        "TDS_status": "Thredds Data Server is not accessible. Contact TDS administrator.",
                    },
                    safe=False,
                )
        except BaseException:
            return JsonResponse(
                {
                    "TDS_status": "Thredds Data Server is not accessible. Contact TDS administrator.",
                },
                safe=False,
            )
