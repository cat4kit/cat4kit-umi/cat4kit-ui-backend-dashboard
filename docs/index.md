<!--
SPDX-FileCopyrightText: 2023 KIT - Karlsruher Institut für Technologie

SPDX-License-Identifier: CC-BY-4.0
-->

<!--
cat4kit-ui-backend-dashboard documentation master file
You can adapt this file completely to your liking, but it should at least
contain the root `toctree` directive.
-->

# Welcome to cat4kit-ui-backend-dashboard's documentation!

[![CI](https://codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-dashboard/badges/main/pipeline.svg)](https://codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-dashboard/-/pipelines?page=1&scope=all&ref=main)
[![Code coverage](https://codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-dashboard/badges/main/coverage.svg)](https://codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-dashboard/-/graphs/main/charts)
<!-- TODO: uncomment the following line when the package is registered at https://readthedocs.org -->
<!-- [![Docs](https://readthedocs.org/projects/cat4kit-ui-backend-dashboard/badge/?version=latest)](https://cat4kit-ui-backend-dashboard.readthedocs.io/en/latest/) -->
[![Latest Release](https://codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-dashboard/-/badges/release.svg)](https://codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-dashboard)
<!-- TODO: uncomment the following line when the package is published at https://pypi.org -->
<!-- [![PyPI version](https://img.shields.io/pypi/v/cat4kit-ui-backend-dashboard.svg)](https://pypi.python.org/pypi/cat4kit-ui-backend-dashboard/) -->
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![PEP8](https://img.shields.io/badge/code%20style-pep8-orange.svg)](https://www.python.org/dev/peps/pep-0008/)
[![Checked with mypy](http://www.mypy-lang.org/static/mypy_badge.svg)](http://mypy-lang.org/)
<!-- TODO: uncomment the following line when the package is registered at https://api.reuse.software -->
<!-- [![REUSE status](https://api.reuse.software/badge/codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-dashboard)](https://api.reuse.software/info/codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-dashboard) -->

**The Cat4KIT UI Backend Dashboard functions as an account management application, API monitoring bridge, and URL validator, providing a comprehensive suite of tools for efficient backend management.**

```{warning}

This page has been automatically generated as has not yet been reviewed
by the authors of cat4kit-ui-backend-dashboard! Stay tuned for
updates and discuss with us at <https://codebase.helmholtz.cloud/cat4kit/cat4kit-umi/cat4kit-ui-backend-dashboard>
```

```{toctree}
---
maxdepth: 2
caption: "Contents:"
---
installation
configuration
api
contributing
```

## How to cite this software

```{eval-rst}
.. card:: Please do cite this software!

   .. tab-set::

      .. tab-item:: APA

         .. citation-info::
            :format: apalike

      .. tab-item:: BibTex

         .. citation-info::
            :format: bibtex

      .. tab-item:: RIS

         .. citation-info::
            :format: ris

      .. tab-item:: Endnote

         .. citation-info::
            :format: endnote

      .. tab-item:: CFF

         .. citation-info::
            :format: cff
```

# License information

Copyright © 2023 KIT - Karlsruher Institut für Technologie

The source code of cat4kit-ui-backend-dashboard is licensed under EUPL-1.2.

If not stated otherwise, the contents of this documentation is licensed
under CC-BY-4.0.

## Indices and tables

-   {ref}`genindex`
-   {ref}`modindex`
-   {ref}`search`
