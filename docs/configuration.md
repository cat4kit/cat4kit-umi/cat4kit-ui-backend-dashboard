<!--
SPDX-FileCopyrightText: 2023 KIT - Karlsruher Institut für Technologie

SPDX-License-Identifier: CC-BY-4.0
-->

(configuration)=

# Configuration options

## Configuration settings

The following settings in your djangos `settings.py` have an effect on the app:

```{eval-rst}
.. automodulesumm:: cat4kit_ui_backend_dashboard.app_settings
    :autosummary-no-titles:
```
